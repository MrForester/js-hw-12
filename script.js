const keys = document.querySelectorAll(".btn");
document.addEventListener("keyup", keyManager);

function keyManager(event) {
  keys.forEach(function element(btn) {
    if (event.key.toLowerCase() === btn.innerText.toLowerCase()) {
      btn.classList.add("focus-color");
    } else {
      btn.classList.remove("focus-color");
    }
  });
};
